#!/usr/bin/env python

import subprocess, re, sys
from netaddr import IPAddress, IPNetwork

PATTERN = '^\d+:\s+(\w+)\s+inet\s+(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})' #regex to pull interface name and ip address from 'ip -o -4 -a'

if len(sys.argv) < 2:
  exit(1)

interfaces = map(lambda x: re.search(PATTERN, x).groups(),  subprocess.check_output(['ip', '-o', '-4', 'a']).split('\n')[:-1])
for i in interfaces:
  if IPAddress(i[1]) in IPNetwork(sys.argv[1]):
    sys.stdout.write(i[0])
    sys.stdout.flush()
    exit(0)
